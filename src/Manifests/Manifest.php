<?php

namespace JournoLink\ComposerState\Manifests;

class Manifest
{
    protected array $items;

    public function __construct(array $items)
    {
        $this->items = $items;
    }

    /**
     * Determine if this and the given manifest contents match.
     *
     * @param  \JournoLink\ComposerState\Manifests\Manifest $manifest
     * @return bool
     */
    public function matches(Manifest $manifest): bool
    {
        return $this->toHash() === $manifest->toHash();
    }

    /**
     * Diff this and the given manifest.
     *
     * @param \JournoLink\ComposerState\Manifests\Manifest $manifest
     * @return \JournoLink\ComposerState\Manifests\Manifest
     */
    public function diff(Manifest $manifest): Manifest
    {
        return new self(
            array_diff($this->toArray(), $manifest->toArray())
        );
    }

    /**
     * Return the number of items in the manifest.
     *
     * @return int
     */
    public function count(): int
    {
        return count($this->items);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->items;
    }

    /**
     * Return an MD5 hash of the contents of the manifest.
     *
     * @return string
     */
    public function toHash(): string
    {
        $items = $this->toArray();
        ksort($items);

        return md5(
            json_encode($items)
        );
    }
}
