<?php

namespace JournoLink\ComposerState\Manifests;

use RuntimeException;

class InstalledManifest extends Manifest
{
    /**
     * Populate the manifest from the specified file on disk.
     *
     * @param string $file
     * @return static
     */
    public static function loadFromFile(string $file): self
    {
        if (!file_exists($file)) {
            throw new RuntimeException('Manifest file does not exist');
        }

        $result = [];
        foreach (json_decode(file_get_contents($file), true) as $entry) {
            $result[$entry['name']] = $entry['version'];
        }

        return new self(
            $result
        );
    }
}
