<?php

namespace JournoLink\ComposerState;

use JournoLink\ComposerState\Manifests\RequiredManifest;
use JournoLink\ComposerState\Manifests\InstalledManifest;

class State
{
    protected InstalledManifest $installed;
    protected RequiredManifest $required;

    public function __construct(string $installedJson, string $composerLock)
    {
        $this->installed = InstalledManifest::loadFromFile($installedJson);
        $this->required = RequiredManifest::loadFromFile($composerLock);
    }

    /**
     * Validate the currently installed packages against those required.
     *
     * @return bool
     */
    public function isValid(): bool
    {
        return $this->installed->matches($this->required);
    }
}
