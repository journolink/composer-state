<?php

namespace JournoLink\ComposerState\Laravel;

use JournoLink\ComposerState\State;
use Illuminate\Support\ServiceProvider;
use JournoLink\ComposerState\Laravel\Exceptions\ComposerInstalledRequiredException;

class ComposerStateServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../../config/config.php', 'composer-state');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../../config/config.php' => config_path('composer-state.php'),
            ], 'config');
        }

        $config = $this->app['config']->get('composer-state');

        if ($config['enabled']) {
            $state = new State($config['installed'], $config['lock']);

            if (!$state->isValid()) {
                throw ComposerInstalledRequiredException::make();
            }
        }
    }
}
