<?php

namespace JournoLink\ComposerState\Laravel\Exceptions;

use Exception;

class ComposerInstalledRequiredException extends Exception
{
    public static function make(): self
    {
        return new self(
            'The installed composer dependencies do not match those within the lock file. Run composer install to continue.'
        );
    }
}
