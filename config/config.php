<?php

return [

    'enabled' => env('APP_DEBUG', false),

    'installed' => env('COMPOSER_STATE_INSTALLED', base_path('vendor/composer/installed.json')),

    'lock' => env('COMPOSER_STATE_LOCK', base_path('composer.lock')),

];
