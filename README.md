# Composer State

 [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![pipeline status](https://gitlab.com/journolink/composer-state/badges/master/pipeline.svg)](https://gitlab.com/journolink/composer-state/-/commits/master)
[![coverage report](https://gitlab.com/journolink/composer-state/badges/master/coverage.svg)](https://gitlab.com/journolink/composer-state/-/commits/master)


Get notified if you're installed Composer packages don't match those required by the lock file. 

Useful when working between branches to ensure you're working with the correct set of dependencies.

The `State::isValid()` method will return false when the packages and versions in your `installed.json` file do not match those in your `composer.lock` file.

## Installation
```
composer require --dev journolink/composer-state
```

## Laravel

For Laravel projects, the package includes a Service Provider to automatically initialise the State validator. In the event the validator returns false during the app boot phase, a `ComposerInstallRequiredException` will be thrown.

### Initialisation

In a Laravel 5.5+ environment, the Service Provider will be automatically discovered during `php artisan package:discover`

### Configuration

To publish the configuration to the application, run the following command
```
php artisan vendor:publish --provider="JournoLink\ComposerState\Laravel\ComposerStateServiceProvider"
```
By default, the package is active when the `APP_DEBUG` env value is set to `true`, however this can be overridden using the `enabled` key within the config file.

The config file contains default locations for both the `installed.json` and `composer.lock` files. Both are overridable within the config file or using the relevant environment variables.


## Manual Usage

```php
use JournoLink\ComposerState\State;

$state = new State(
    'path/to/installed.json',
    'path/to/composer.lock'
);

if (!$state->isValid()) {
    throw new \Exception('Composer state is invalid');
}
```
