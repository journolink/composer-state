<?php

namespace JournoLink\ComposerState\Tests;

use PHPUnit\Framework\TestCase;
use JournoLink\ComposerState\State;

class StateTest extends TestCase
{
    public function testIsValidReturnsTrueWithMatchedDependencies(): void
    {
        $state = new State(
            __DIR__ .'/fixtures/installed1.json',
            __DIR__ .'/fixtures/composerlock1.json'
        );
        
        $this->assertTrue($state->isValid());
    }

    public function testIsValidReturnsFalseWithMismatchedDependencies(): void
    {
        $state = new State(
            __DIR__ .'/fixtures/installed2.json',
            __DIR__ .'/fixtures/composerlock1.json'
        );

        $this->assertFalse($state->isValid());

        $state = new State(
            __DIR__ .'/fixtures/installed1.json',
            __DIR__ .'/fixtures/composerlock2.json'
        );

        $this->assertFalse($state->isValid());
    }

    public function testIsValidReturnsFalseWithPartialMismatchedDependencies(): void
    {
        $state = new State(
            __DIR__ .'/fixtures/installed3.json',
            __DIR__ .'/fixtures/composerlock1.json'
        );

        $this->assertFalse($state->isValid());

        $state = new State(
            __DIR__ .'/fixtures/installed1.json',
            __DIR__ .'/fixtures/composerlock3.json'
        );

        $this->assertFalse($state->isValid());
    }
}
