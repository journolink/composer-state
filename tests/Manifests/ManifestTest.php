<?php

namespace JournoLink\ComposerState\Tests\Manifests;

use PHPUnit\Framework\TestCase;
use JournoLink\ComposerState\Manifests\Manifest;

class ManifestTest extends TestCase
{
    public function testCountReturnsNumbersOfItemsInManifest(): void
    {
        $manifest = new Manifest([1, 2, 3, 4, 5]);

        $this->assertEquals(5, $manifest->count());
    }

    public function testManifestCanBeReturnedAsAnArray(): void
    {
        $manifest = new Manifest([1, 2, 3, 4, 5]);

        $this->assertEquals([1, 2, 3, 4, 5], $manifest->toArray());
    }

    public function testManifestCanBeDiffedAgainstAnother(): void
    {
        $manifest = new Manifest([1, 2, 3, 4, 5]);
        $manifest2 = new Manifest([2, 3, 4, 5, 6]);

        $this->assertInstanceOf(Manifest::class, $manifest->diff($manifest2));
        $this->assertEquals([0 => 1], $manifest->diff($manifest2)->toArray());
        $this->assertEquals([4 => 6], $manifest2->diff($manifest)->toArray());
    }

    public function testManifestHashCanBeGenerated(): void
    {
        $manifest = new Manifest(['one' => 1.1, 'two' => 2.2, 'three' => 3.3]);

        $this->assertEquals('97374b2b07570ab5b358d292e017aa1b', $manifest->toHash());
    }
}
